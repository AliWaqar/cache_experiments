# -*- coding: utf-8 -*-
"""
Created on Wed May 25 19:04:56 2016

@author: Skidro

@description: This file is a customized version of the script for plotting page distribution
              graphs for Nemo (Xeon-E5 2608 v3) Machine
"""
import numpy as np
import matplotlib.pyplot as plt

#==============================================================================
# setup()
# Function for defining the setup of the experiment at hand
#==============================================================================
def setup():
	global sys_page_size_kb, num_color_bins, color_threshold_per_bin
	llc_size_kb = 15360
	llc_ways = 20
	llc_private_ways = 4
	num_color_bins = 32
	sys_page_size_kb = 4
 
	llc_way_size = llc_size_kb / llc_ways
	colors_per_way = llc_way_size / sys_page_size_kb
	private_colors = colors_per_way * llc_private_ways
	color_threshold_per_bin = private_colors / num_color_bins
 
	return
    
#==============================================================================
# parse_file()
# Function for parsing a single file for collecting and aggregating experiment data
# @filename : Name of file to be parsed
# @max_file : Name of the file which contains the max miss-rate stats
# @min_file : Name of the file which contains the min miss-rate stats    
#==============================================================================
def parse_file(filename, max_file, min_file):
	# Open the result file
	try:
		fd = open(filename, "r")
	
	except:
		print "Error opening file!"
		exit()
	
	
	# Continue processing the file
	lines		= fd.readlines()
	
	data_hash	= {}
	colors		= []
	pages		= []
	
	# Parse the lines
	for line in lines:
		if 'miss-rate' in line:
			mrate = line.split(' ')[-1]
			try:
				mrate = float(mrate[:-2])
    				
				# Check if this miss-rate is currently smallest or largest        				
				if mrate > max_file[1]:	        
					max_file[1] = mrate
					max_file[0] = filename
				if mrate < min_file[1]:
					min_file[1] = mrate
					min_file[0] = filename        
    
			except:
				print ("Error converting miss-rate (%s) to float!") % mrate
				exit()
		
		if 'average' in line:
			words = line.split(' ')
	
			tgt = 0
			for word in words:
				try:
					if tgt == 0:
						bw = float(word)
						tgt += 1
					else:
						latency = float(word)
						break
				except:
					pass
	
		if 'color' in line:
			words = line.split(' ')
			color = ''
			for word in words:
				if 'color' in word:
					for letter in word:
						try:
							temp = int(letter)
							temp += 1       
							color += letter
						except:
							pass
				else:
					try:
						page_bin = int(word)
						break
					except:
						pass
			color = int(color)
			colors.append(color)
			pages.append(page_bin)
	
	data_hash["color-list"]	= colors
	data_hash["page-list"]	= pages
	data_hash["miss-rate"]	= mrate
	data_hash["bandwidth"]	= bw
	data_hash["latency"]	= latency
	
	# Close the file now
	fd.close()

	# Return results to caller
	return data_hash
			
#==============================================================================
# fig_subplot()
# Plots the given data into a subplot in the given postion of the figure
# @ data : Dictionary of data-set to be plotted
# @ pos  : Position of subplot of data-set in the output figure
#==============================================================================
def fig_subplot(data, pos):
    # Split the plot into bottom half and top half
    ut_map = [33.33, 33.33, 50, 50, 66.67, 66.67, 83.33, 83.33, 100, 100]
    values = np.array(data["page-list"])
    above_threshold = np.maximum(values - color_threshold_per_bin, 0)
    below_threshold = np.minimum(values, color_threshold_per_bin)
    
    # Calculate the actual working set size using the number of pages
    data["WS"] = values.sum() * sys_page_size_kb / 1024

    # Calculate Partition utilization for this data set
    utilization = ut_map[(pos % 10) - 1]
    
    # Make the bar plot for page -> color distribution for top data
    if pos > 1000:    
        plt.subplot(5, 2, 10)
    else:
        plt.subplot(pos)
    plt.bar(data["color-list"], values, 1, color = "g")
    #plt.bar(data["color-list"], above_threshold, 1, color = "r", bottom = below_threshold)
    #plt.plot([0., num_color_bins], [color_threshold_per_bin, color_threshold_per_bin], "k--", label = 'Page Threshold per Bin : ' + str(color_threshold_per_bin))
    plt.ylim(0, 3*color_threshold_per_bin)
    plt.xlim(0, num_color_bins)
    if pos == 519 or pos == 5110:
        plt.xlabel('Page Color Bins', fontsize = 20)
    plt.ylabel('Number of Pages', fontsize = 20)
    #plt.legend()
    title = 'UT : ' + str(utilization) + '% | BW : ' + str(data["bandwidth"]) + ' MB/s | LT : ' + str(data["latency"]) + ' ns | MR : ' + str(data["miss-rate"]) + '%'
    plt.title(title, fontsize = 20)
    
    return
    
#==============================================================================
# plot_data()
# Primary function for plotting data
# @data_hash_list : A list of dictionaries of data to be plotted
# @runs           : Number of hashes in the data list
#==============================================================================
def plot_graphs(data_hash_list, runs):
    # Create one figure to contain all subplots
    fig = plt.figure(1, figsize = (45, 30), dpi = 500)
    title = 'Page Distribution in Different Color Bins - Max vs Min Miss-Rate Scenario'
    plt.suptitle(title, fontsize = 25)

    
    for run in range(0, runs):
        place = int('52' + str(run + 1))
        
        # Plot the data-set
        try:
            fig_subplot(data_hash_list[run], place)
        except:
            print ('Unable to plot data for run : %d | place : %d') % (run, place)
            exit()

    # Create a name for saving the plot figure
    fig_name = 'page_distribution_xeon.png'
    fig.savefig(fig_name)
    
    return

#==============================================================================
# main()
# This function is responsible for calling the helper functions in this file for parsing and
# plotting experiment data obtained for way-partitioning
#==============================================================================
    
def main():
	# Create the file-name for the data-set to be parsed
	dir_list	= []
	ws_size	= [1024, 1536, 2048, 2560, 3072]
	ps_size	= 3072
  	max_run 	= 1000
	run_data	= []
	ws_data	= []
	all_data	= []
	limits_data = []
	max_file = {}
	min_file = {} 
 
	# Prepare the list of directories to be parsed
	dir_list.append('data/')
 
	# Call the setup function to initialize global data
	setup()
 
	# Parse the data in each directory 
	for folder in dir_list:
		for ws in ws_size:
  			# Initialize trackers for largest and smallest miss-rate runs      
  			max_file[ws] = ['foo', 0.0]
  			min_file[ws] = ['foo', 100.0]
  			for run in range(1, max_run + 1): 			
 				file_name		= folder + 'l' + str(ws) + '_r' + str(run) + '.log'   
				parsed_data		= parse_file(file_name, max_file[ws], min_file[ws])
				parsed_data["PS"]	= float(ps_size) / 1024
				run_data.append(parsed_data)
			ws_data.append(run_data)
			run_data = []               
		all_data.append(ws_data)
		ws_data = []
  
		# Parse the files which contain the max and min run stats for each working set
		for ws in ws_size:
			max_hless = ['foo', 0]
			min_hless = ['foo', 100]      
			parsed_max_data = parse_file(max_file[ws][0], max_hless, min_hless)
			parsed_min_data = parse_file(min_file[ws][0], max_hless, min_hless)
			parsed_max_data["PS"] = float(ps_size)/1024
   			parsed_min_data["PS"] = float(ps_size)/1024
   			parsed_max_data["WS"] = float(ws)/1024
   			parsed_min_data["WS"] = float(ws)/1024      
			limits_data.append(parsed_max_data)   
			limits_data.append(parsed_min_data)
   
	# Plot the graphs of the parsed data	
 	plot_graphs(limits_data, 10)
 
 	return
	
#==============================================================================
# Call the main function when this file is invoked
#==============================================================================
if __name__ == "__main__":
	main()
