RUNS=1000
TGT_MEM=3072
STR_MEM=1024
STP_MEM=512
vars=l
varm=_r

# Run the bandwidth benchmark with real=time priority for one-million iterations
until [ $STR_MEM -gt $TGT_MEM ]; do
	until [ $RUNS -lt 1 ]; do
		echo ">> Working Set : $STR_MEM | Run : $RUNS"
		./bandwidth -c 0 -m $STR_MEM -i 10000 -t 1000 -p -19 &> results/$vars$STR_MEM$varm$RUNS.log
		let RUNS-=1
	done
	let RUNS=1000
	let STR_MEM+=STP_MEM
done
