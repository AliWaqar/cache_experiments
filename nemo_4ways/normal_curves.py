# -*- coding: utf-8 -*-
"""
Created on Wed May 25 19:04:56 2016

@author: Skidro

@description: This file is a customized version of the script for plotting normal curves
              for Nemo (Xeon-E5 2608 v3) Machine
"""

import numpy as np
import pylab as pl
import scipy.stats as stats

#==============================================================================
# parse_file()
# Function for parsing a single file for collecting and aggregating experiment data
# @filename : Name of file to be parsed
#==============================================================================
def parse_file(filename):
	# Open the result file
	try:
		fd = open(filename, "r")
	
	except:
		print "Error opening file!"
		exit()
	
	
	# Continue processing the file
	lines		= fd.readlines()
	
	data_hash	= {}
	colors		= []
	pages		= []
	
	# Parse the lines
	for line in lines:
		if 'miss-rate' in line:
			mrate = line.split(' ')[-1]
			try:
				mrate = float(mrate[:-2])
			except:
				print ("Error converting miss-rate (%s) to float!") % mrate
				exit()
		
		if 'average' in line:
			words = line.split(' ')
	
			tgt = 0
			for word in words:
				try:
					if tgt == 0:
						bw = float(word)
						tgt += 1
					else:
						latency = float(word)
						break
				except:
					pass
	
		if 'color' in line:
			words = line.split(' ')
			color = ''
			for word in words:
				if 'color' in word:
					for letter in word:
						try:
							temp = int(letter)
							temp += 1
							color += letter
						except:
							pass
				else:
					try:
						page_bin = int(word)
						break
					except:
						pass
			color = int(color)
			colors.append(color)
			pages.append(page_bin)
	
	data_hash["color-list"]	= colors
	data_hash["page-list"]	= pages
	data_hash["miss-rate"]	= mrate
	data_hash["bandwidth"]	= bw
	data_hash["latency"]	= latency
	
	# Close the file now
	fd.close()

	# Return results to caller
	return data_hash

#==============================================================================
# plot_data()
# Primary function for plotting data
# @data_hash_list : A list of dictionaries of all the data
# @runs           : Number of hashes in the data list
#==============================================================================
def plot_normal_curves(all_data, max_run):
	# Create one figure to contain all subplots
	fig	= pl.figure(1, figsize = (15, 20))
	title	= 'Probability Density Curves for Cache Miss-Rate : Nemo [Xeon-E5 2608 v3]'
	pl.suptitle(title, fontsize = 25)
 
 	# Create a map for the subplot
	map = [1, 2, 3, 4, 5]

	place	= 0
	for ws_data in all_data:
		for ws in ws_data:    
			pos = int('51' + str(map[place]))
			plot_curve(ws, max_run, pos)
			place += 1
   
	# Create a name for saving the plot figure
	fig_name = 'Normal_Curves_Scaled' + '.png'
	fig.savefig(fig_name)

	return
			

#==============================================================================
# plot_curve()
# Primary function for plotting data
# @data_hash_list : A list of dictionaries of all the data
# @runs           : Number of items in the data list
#==============================================================================
def plot_curve(data_hash_list, runs, place):
	# Create an array for holding miss-rates
	mr_array = []
 
	# Create a map for working values
	ws_map = [1024, 1536, 2048, 2560, 3072]

	# Populate the array
 	for run in range(0, runs):
 		mr_array.append(data_hash_list[run]["miss-rate"])


	# Sort the data
	mr_array = sorted(mr_array)
	
 	# Calculate mean and standard deviation of the data
	mr_mean = np.mean(mr_array)
	mr_std  = np.std(mr_array)
 
	# Fit a normal curve on the sorted data
	fit = stats.norm.pdf(mr_array, mr_mean, mr_std)

	# Create a subplot for this graph in the canvas
	pl.subplot(place)

	# Plot the normal curve
	pl.plot(mr_array, fit, '-o')

	# Plot the histogram along the curve
	pl.hist(mr_array, normed = True)
 
	# Specify labels for the axes
	if place == 515: 
		pl.xlabel('LLC Miss-Rate', fontsize = 15)
	pl.ylabel('Probability', fontsize = 15)
 
	# Set limits for the axes
	pl.xlim(0, 40)
#	pl.ylim(0, 0.25) 

	# Display the plot
	title = 'Utilization : ' + str(((ws_map[(place % 10) - 1]) / 3072.0) * 100) + '% | Mean : ' + str(mr_mean) + ' | Standard Deviation : ' + str(mr_std)
	pl.title(title, fontsize = 15)

	return

#==============================================================================
# main()
# This function is responsible for calling the helper functions in this file for parsing and
# plotting experiment data obtained for way-partitioning
#==============================================================================
    
def main():
	# Create the file-name for the data-set to be parsed
	dir_list	= []
	ws_size	= [1024, 1536, 2048, 2560, 3072]
	ps_size	= 3072
  	max_run 	= 1000
	run_data	= []
	ws_data	= []
	all_data	= []
 
	# Prepare the list of directories to be parsed
	dir_list.append('data/')
 
	# Parse the data in each directory 
	for folder in dir_list:
		for ws in ws_size:
  			for run in range(1, max_run + 1): 			
 				file_name		= folder + 'l' + str(ws) + '_r' + str(run) + '.log'   
				parsed_data		= parse_file(file_name)
				parsed_data["PS"]	= ps_size / 1024
				run_data.append(parsed_data)
			ws_data.append(run_data)
			run_data = []               
		all_data.append(ws_data)
		ws_data = []  
	
	# Plot the graphs of the parsed data	
	plot_normal_curves(all_data, max_run)
 
 	return
	
#==============================================================================
# Call the main function when this file is invoked
#==============================================================================
if __name__ == "__main__":
	main()
